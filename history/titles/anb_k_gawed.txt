﻿k_gawed = {
	1000.1.1 = { change_development_level = 8 }
	# 800.1.1 = {
		# succession_laws = { male_only_law }
	# }
	471.2.18 = { # Gawedi conquest of Vertesk, foundation of the kingdom
		holder = gawe_0001 # Godrac I "the Invader" Gawe
	}
	486.5.29 = {
		holder = gawe_0002 # Godrac II Gawe
	}
	530.2.26 = {
		holder = gawe_0006 # Godrac III Gawe
	}
	547.8.1 = {
		holder = gawe_0007 # Ulric I Gawe
	}
	548.12.2 = {
		holder = gawe_0008 # Godryc I Gawe
	}
	562.6.30 = {
		holder = gawe_0009 # Godrac IV "the Old" Gawe
	}
	638.11.10 = {
		holder = gawe_0011 # Godrac V Gawe
	}
	685.4.9 = {
		holder = gawe_0012 # Carlan I Gawe
	}
	689.1.9 = {
		holder = gawe_0013 # Godwin I Gawe
	}
	708.10.1 = {
		holder = gawe_0015 # Godrac VI Gawe
	}
	745.8.13 = {
		holder = gawe_0016 # Godryc II Gawe
	}
	771.10.26 = {
		holder = gawe_0017 # Godrac VII Gawe
	}
	791.3.25 = {
		holder = gawe_0018 # Alenn I Gawe
	}
	822.5.6 = {
		holder = gawe_0019 # Godrac VIII Gawe
	}
	839.6.9 = {
		holder = gawe_0020 # Godryc III Gawe
	}
	864.7.17 = {
		holder = gawe_0021 # Ulric II Gawe
	}
	873.6.19 = {
		holder = gawe_0022 # Alenn II Gawe
	}
	910.11.30 = {
		holder = gawe_0023 # Godrac IX Gawe
	}
	923.12.27 = {
		holder = gawe_0024 # Carlan II Gawe
	}
	950.4.7 = {
		holder = gawe_0025 # Ulric III Gawe
	}
	956.12.4 = {
		holder = gawe_0026 # Godrac X Gawe
	}
	986.5.23 = {
		holder = gawe_0027 # Alenn III Gawe
	}
}

d_vertesk = {
	986.1.1 = {
		holder = 521 # Venac
	}
	1000.1.1 = { change_development_level = 9 }
	1014.2.17 = {
		holder = 0 # placeholder
	}
}

c_vertesk = {
	986.1.1 = {
		holder = 521 # Venac
	}
	1000.1.1 = { change_development_level = 17 }
	1014.2.17 = {
		holder = 0 # placeholder
	}
}

d_ginnfield = {
	800.1.1 = {
		liege = k_gawed
	}
	1014.7.23 = {
		holder = alenath_0001 # Tomar síl Alenath
	}
}

c_alenath = {
	1000.1.1 = { change_development_level = 13 }
		1014.7.23 = {
		holder = alenath_0001 # Tomar síl Alenath
	}
}

c_elwick_upon_alen = {
		1014.7.23 = {
		holder = alenath_0001 # Tomar síl Alenath
	}
}

c_ginnfield = { 
	1014.1.1 = {
		liege = "d_ginnfield"
	}
	1014.7.23 = {
		holder = ginnfield_0001 # Humber of Ginnfield
	}
}

c_gaweton = {
	1000.1.1 = { change_development_level = 11 }
}

c_vanbury = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = k_gawed
	}
	923.12.27 = {
		holder = gawe_0024 # Carlan Gawe inherits from his father
	}
	949.3.28 = {
		holder = vanbury_0001 # Ricard of Vanbury gifted by his father for his talents
	}
	982.5.29 = {
		holder = vanbury_0002 # Carlan of Vanbury
	}
	1015.4.10 = {
		holder = vanbury_0003 # Godryc of Vanbury
	}
}

c_drakesford = {
	1000.1.1 = { change_development_level = 7 }
	800.1.1 = {
		liege = "k_gawed"
	}
	986.4.10 = {
		holder = alenic30030 # Garrett Drakesford
	}
	1015.12.14 = {
		holder = alenic30027 # Byron Drakesford
	}
}

c_baldfather = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = "k_gawed"
	}

	1020.1.1 = {
		holder = alenic30033 # Godrac Baldfather
	}
}

c_swinthorpe = {
	1000.1.1 = { change_development_level = 10 }
	800.1.1 = {
		liege = "k_gawed"
	}

	1020.1.1 = {
		holder = alenic30033 # Godrac Baldfather
	}
}

c_gerwick = {
	969.10.16 = {
		liege = k_gawed
		holder = gerwick_0001
	}
	# 990.8.3 = {
		# liege = e_castanor
		# holder = 0
	# }
	1000.1.1 = { change_development_level = 10 }
	1015.7.21 = {
		liege = k_gawed
		holder = gerwick_0002
	}
}

c_alenfield = {
	969.10.16 = {
		liege = k_gawed
		holder = gerwick_0001
	}
	# 990.8.3 = {
		# liege = e_castanor
		# holder = 0
	# }
	1000.1.1 = { change_development_level = 9 }
	1015.7.21 = {
		liege = k_gawed
		holder = gerwick_0002
	}
}

c_morban = {
	969.10.16 = {
		liege = k_gawed
		holder = gerwick_0001
	}
	# 990.8.3 = {
		# liege = e_castanor
		# holder = 0
	# }
	1000.1.1 = { change_development_level = 7 }
	1015.7.21 = {
		liege = k_gawed
		holder = gerwick_0002
	}
}

c_humbercroft = {
	1000.1.1 = { change_development_level = 8 
	}
	995.7.8 = {
		liege = k_gawed
		holder = humbercroft_0002 # Rabard Humbercroft
	}
		1008.10.28 = {
		liege = k_gawed
		holder = humbercroft_0005 # Camor Humbercroft
	}
}

c_oxington = {
	1000.1.1 = { change_development_level = 8 
	}
	995.7.8 = {
		liege = k_gawed
		holder = humbercroft_0002 # Rabard Humbercroft
	}
		1008.10.28 = {
		liege = k_gawed
		holder = humbercroft_0005 # Camor Humbercroft
	}
}

c_exeham = {
	1000.1.1 = { change_development_level = 8 
	}
	995.7.8 = {
		liege = k_gawed
		holder = humbercroft_0002 # Rabard Humbercroft
	}
		1008.10.28 = {
		liege = k_gawed
		holder = humbercroft_0005 # Camor Humbercroft
	}
}

d_westmounts = {
	1018.4.19 = {
		holder = 98 #Toman Wight
		liege = "k_gawed"
	}
}

d_greatmarch = {
	1000.1.1 = { change_development_level = 6 }
}

c_greatwoods = {
	800.1.1 = {
		liege = "k_gawed"
	}
	986.4.10 = {
		holder = alenic30030 # Garrett Drakesford
	}
	1015.12.14 = {
		holder = alenic30027 # Byron Drakesford
	}
}

c_legions_clearing = {
	1000.1.1 = { change_development_level = 8 }
}

d_balvord = {
	1000.1.1 = { change_development_level = 6 }
	994.1.1 = {
		holder = 579 #Petran
		liege = "k_gawed"
	}
}

c_gardfort = {
	994.1.1 = {
		holder = 579 #Petran
		liege = "k_gawed"
	}
}

c_aldtempel = {
	994.1.1 = {
		holder = 579 #Petran
		liege = "k_gawed"
	}
}

c_wolfden = {
	994.1.1 = {
		holder = 579 #Petran
	}
	1021.6.14 = {
		holder = 581 #Jordan
		liege = "d_balvord"
	}
}

d_oudescker = {
	1000.1.1 = { change_development_level = 6 }
	800.1.01 = {
		liege = k_gawed
	}
	979.5.30 = {
		holder = alenic30040 # Oswald Oudescker
	}
}

c_jonsway = {
	1000.1.1 = { change_development_level = 7 }
}

d_arbaran = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

c_arbaran = {
	1000.1.1 = { change_development_level = 10 }
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

c_arca_pirvar = {
	1000.1.1 = { change_development_level = 9 }
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

d_freecestir = {
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

c_freecestir = {
	1000.1.1 = { change_development_level = 12 }
	1015.9.8 = {
		holder = 60 #Ianren the Rider
	}
}

c_elvenaire = {
	1000.1.1 = { change_development_level = 7 }
}

c_mirewatch = {
	1000.1.1 = { change_development_level = 7 }
}
