#k_drolas
##d_arag_drolas
###c_drolaspand
580 = {		#Drolaspand

    # Misc
    culture = drolasesi
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_betelis
577 = {		#Beteliss

    # Misc
    culture = drolasesi
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_drolaklum
574 = {		#Drolaklum

    # Misc
    culture = drolasesi
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_girakub
582 = {		#Azka Girakeru

    # Misc
    culture = east_divenori
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

##d_baru_drolas
###c_dromaz
573 = {		#Dromaz

    # Misc
    culture = drolasesi
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}

###c_kurmod
578 = {		#Kurmod

    # Misc
    culture = drolasesi
    religion = bulwari_sun_cult
	holding = castle_holding

    # History
}