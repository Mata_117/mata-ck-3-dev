#k_heunthume
##d_heunthume
###c_heunthume
660 = {		#Heunthume

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_lisinyalen
663 = {		#Lisinyalen

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_hytiranyalen
661 = {		#Hytiranyalen

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

##d_tousavellen
###c_haqharias
659 = {		#Haqharias

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_jarmevain
658 = {		#Jarmevain

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_setufiraor
662 = {		#Setufiraor

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_hisarytor
657 = {		#Hisarytor

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_tounafira
651 = {		#Tounafira

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

##d_Ayarallen
###c_ayarallen
666 = {		#Ayarallen

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_arnilqan
667 = {		#Arnilqan

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

##d_thissilen
###c_thissilen
2885 = {    #Thissilen

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History

}
539 = {		#Qasnabor

    # Misc
    culture = bahari
    religion = bulwari_sun_cult
	holding = none

    # History
}

###c_nasru_ean
540 = {		#Nasru-ean

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}
2886 = {    #Eduz-Arfaya

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none

    # History

}
2887 = {    #Nasrusad

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none

    # History

}

###c_rianelen
650 = {		#Azkasad

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none

    # History
}
2888 = { #Kyrefiraor

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none

    # History

}
2889 = { #Rianelen

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History

}

###c_daqelum
649 = {		#Daqelum

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}
2890 = {    #Maermyfira

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = none

    # History

}