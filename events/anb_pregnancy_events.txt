﻿namespace = anb_pregnancy

#ANBENNAR
# End pregnancy of incompatible races
anb_pregnancy.0001 = {
	hidden = yes
	
	trigger = {
		NOT = {
			compatible_race_trigger = {
				MOTHER = scope:mother
				FATHER = scope:real_father
			}
		}
	}
	
	immediate = {
		end_pregnancy = yes
	}
}

#ANBENNAR
# Make harpy pregnancies have female children
anb_pregnancy.0002 = {
	hidden = yes
	
	trigger = {
		scope:mother = {
			has_trait = race_harpy
		}
	}
	
	immediate = {
		set_pregnancy_gender = female
	}
}