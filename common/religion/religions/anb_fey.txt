﻿fey_religion = { #TODO, copied from Lencori
	family = rf_precastanorian
	graphical_faith = pagan_gfx
	doctrine = precastanorian_hostility_doctrine 

	#Main Group
	doctrine = doctrine_no_head	
	doctrine = doctrine_gender_male_dominated	
	doctrine = doctrine_pluralism_pluralistic
	doctrine = doctrine_theocracy_temporal

	#Marriage
	doctrine = doctrine_monogamy
	doctrine = doctrine_divorce_approval
	doctrine = doctrine_bastardry_legitimization
	doctrine = doctrine_consanguinity_cousins

	#Crimes
	doctrine = doctrine_homosexuality_shunned
	doctrine = doctrine_adultery_men_shunned
	doctrine = doctrine_adultery_women_shunned
	doctrine = doctrine_kinslaying_close_kin_crime
	doctrine = doctrine_deviancy_shunned
	doctrine = doctrine_witchcraft_accepted
	#Clerical Functions
	doctrine = doctrine_clerical_function_recruitment
	doctrine = doctrine_clerical_gender_male_only
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	
	#Allow pilgrimages
	doctrine = doctrine_pilgrimage_encouraged
	
	traits = {	
		virtues = {
			brave
			drunkard
			diligent
			lifestyle_traveler
		}
		sins = {
			craven
			temperate
			lazy
		}
	}

	custom_faith_icons = {	#TODO
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = {	
		{ name = "holy_order_riders_of_artanos" }
		{ name = "holy_order_smiths_of_asmirethin" }
		{ name = "holy_order_dioue_guardians" }
	}

	holy_order_maa = { praetorian }	#todo

	localization = {
		#HighGod - Artanos
		HighGodName = lencori_high_god_name
		HighGodNamePossessive = lencori_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_HE
		HighGodHerselfHimself = CHARACTER_HIMSELF
		HighGodHerHis = CHARACTER_HERHIS_HIS
		HighGodNameAlternate = lencori_high_god_name_alternate
		HighGodNameAlternatePossessive = lencori_high_god_name_alternate_possessive

		#Creator - Asmirethin
		CreatorName = lencori_creator_god_name
		CreatorNamePossessive = lencori_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_HE
		CreatorHerHis = CHARACTER_HERHIS_HIS
		CreatorHerHim = CHARACTER_HERHIM_HIM

		#HealthGod - Sorbodua
		HealthGodName = lencori_health_god_name
		HealthGodNamePossessive = lencori_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_SHE
		HealthGodHerHis = CHARACTER_HERHIS_HER
		HealthGodHerHim = CHARACTER_HERHIM_HER
		
		#FertilityGod - Merisse
		FertilityGodName = lencori_fertility_god_name
		FertilityGodNamePossessive = lencori_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_SHE
		FertilityGodHerHis = CHARACTER_HERHIS_HER
		FertilityGodHerHim = CHARACTER_HERHIM_HER

		#WealthGod - Careslobos
		WealthGodName = lencori_wealth_god_name
		WealthGodNamePossessive = lencori_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_HE
		WealthGodHerHis = CHARACTER_HERHIS_HIS
		WealthGodHerHim = CHARACTER_HERHIM_HIM

		#HouseholdGod - Belovira
		HouseholdGodName = lencori_household_god_name
		HouseholdGodNamePossessive = lencori_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_SHE
		HouseholdGodHerHis = CHARACTER_HERHIS_HER
		HouseholdGodHerHim = CHARACTER_HERHIM_HER

		#FateGod - Trovecos
		FateGodName = lencori_fate_god_name
		FateGodNamePossessive = lencori_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_HE
		FateGodHerHis = CHARACTER_HERHIS_HIS
		FateGodHerHim = CHARACTER_HERHIM_HIM

		#KnowledgeGod - Damarta
		KnowledgeGodName = lencori_knowledge_god_name
		KnowledgeGodNamePossessive = lencori_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_SHE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HER
		KnowledgeGodHerHim = CHARACTER_HERHIM_HER

		#WarGod - Artanos
		WarGodName = lencori_high_god_name
		WarGodNamePossessive = lencori_high_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_HE
		WarGodHerHis = CHARACTER_HERHIS_HIS
		WarGodHerHim = CHARACTER_HERHIM_HIM

		#TricksterGod - Dercanos
		TricksterGodName = lencori_trickster_god_name
		TricksterGodNamePossessive = lencori_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod - Artanos
		NightGodName = cannorian_pantheon_night_god_name
		NightGodNamePossessive = cannorian_pantheon_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_HE
		NightGodHerHis = CHARACTER_HERHIS_HIS
		NightGodHerHim = CHARACTER_HERHIM_HIM

		#WaterGod - Sorbodua
		WaterGodName = lencori_health_god_name
		WaterGodNamePossessive = lencori_health_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_SHE
		WaterGodHerHis = CHARACTER_HERHIS_HER
		WaterGodHerHim = CHARACTER_HERHIM_HER


		PantheonTerm = religion_the_eidoueni
		PantheonTermHasHave = pantheon_term_have
		GoodGodNames = {
			lencori_high_god_name
			lencori_high_god_name_alternate
			lencori_creator_god_name
			lencori_water_god_name
			lencori_knowledge_god_name
			lencori_war_god_name
			lencori_household_god_name
			lencori_fate_god_name
			lencori_fertility_god_name
			lencori_health_god_name
		}
		
		
		DevilName = lencori_devil_name
		DevilNamePossessive = lencori_devil_name_possessive
		DevilSheHe = CHARACTER_SHEHE_HE
		DevilHerHis = CHARACTER_HERHIS_HIS
		DevilHerselfHimself = lencori_devil_herselfhimself
		EvilGodNames = {
			lencori_devil_name
		}
		HouseOfWorship = lencori_house_of_worship
		HouseOfWorshipPlural = lencori_house_of_worship_plural
		ReligiousSymbol = lencori_religious_symbol
		ReligiousText = lencori_religious_text
		ReligiousHeadName = lencori_religious_head_title
		ReligiousHeadTitleName = lencori_religious_head_title_name
		DevoteeMale = lencori_devotee_male
		DevoteeMalePlural = lencori_devotee_male_plural
		DevoteeFemale = lencori_devotee_female
		DevoteeFemalePlural = lencori_devotee_female_plural
		DevoteeNeuter = lencori_devotee_neuter
		DevoteeNeuterPlural = lencori_devotee_neuter_plural
		PriestMale = lencori_priest
		PriestMalePlural = lencori_priest_plural
		PriestFemale = lencori_priest
		PriestFemalePlural = lencori_priest_plural
		PriestNeuter = lencori_priest
		PriestNeuterPlural = lencori_priest_plural
		AltPriestTermPlural = lencori_priest_term_plural
		BishopMale = lencori_bishop
		BishopMalePlural = lencori_bishop_plural
		BishopFemale = lencori_bishop
		BishopFemalePlural = lencori_bishop_plural
		BishopNeuter = lencori_bishop
		BishopNeuterPlural = lencori_bishop_plural
		DivineRealm = lencori_divine_realm
		PositiveAfterLife = lencori_positive_afterlife
		NegativeAfterLife = lencori_negative_afterlife
		DeathDeityName = lencori_death_name
		DeathDeityNamePossessive = lencori_death_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERHIS_HIS
		WitchGodName = lencori_good_god_damasta
		WitchGodHerHis = CHARACTER_HERHIS_HER
		WitchGodSheHe = CHARACTER_SHEHE_SHE
		WitchGodHerHim = CHARACTER_HERHIM_HER
		WitchGodMistressMaster = mistress
		WitchGodMotherFather = mother

		GHWName = ghw_purification
		GHWNamePlural = ghw_purifications
	}

	faiths = {
		fey_cult = {
			color = { 20 166 0 }
			icon = the_dame
			
		}
	}
}
		