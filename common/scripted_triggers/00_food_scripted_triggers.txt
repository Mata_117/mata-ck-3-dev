﻿# Anbennar: commeting vanilla stuff

# James _insisted_ I make this file. So here we are.

##################################################
# VEGETABLE TRIGGERS

province_has_vegetable_carrot_trigger = {
	# OR = {
		# geographical_region = world_india
		# geographical_region = world_middle_east
		# geographical_region = world_europe
		# geographical_region = world_steppe_west
		# geographical_region = world_asia_minor
	# }
	# Anbennar
	OR = {
		geographical_region = world_cannor
		geographical_region = world_sarhal_bulwar
	}
}

province_has_vegetable_parsnip_trigger = {
	# NOT = { geographical_region = world_africa }
	# Anbennar
	OR = {
		geographical_region = world_cannor
		geographical_region = world_sarhal_bulwar
	}
}

province_has_vegetable_turnip_trigger = {
	# NOT = { geographical_region = world_africa }
	# Anbennar
	OR = {
		geographical_region = world_cannor
		geographical_region = world_sarhal_bulwar
	}
}

province_has_vegetable_yams_trigger = {
	# OR = {
		# # African yams
		# geographical_region = world_africa
		# # Chinese yams
		# geographical_region = world_tibet
		# geographical_region = world_burma
		# geographical_region = world_steppe_east
		# geographical_region = world_steppe_tarim
	# }
	# Anbennar
	geographical_region = world_sarhal_salahad
}

province_has_vegetable_kokoro_trigger = {
	# geographical_region = world_africa
	# Anbennar
	geographical_region = world_sarhal_salahad
}

province_has_vegetable_ginger_trigger = {
	# OR = {
		# geographical_region = world_india
		# geographical_region = world_burma
	# }
	# Anbennar
	always = no # until we add rahen
}

province_has_vegetable_taro_trigger = {
	# OR = {
		# geographical_region = world_india
		# geographical_region = world_burma
		# geographical_region = world_tibet
		# geographical_region = world_steppe_east
		# geographical_region = world_steppe_tarim
	# }
	# Anbennar
	always = no # until we add rahen
}
