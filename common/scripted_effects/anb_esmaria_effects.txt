﻿

unite_kingdom_of_havoric_decision_effect= {
	show_as_tooltip = {
		title:d_asheniande = {
			set_de_jure_liege_title = title:k_havoric
		}
		title:d_bennon = {
			set_de_jure_liege_title = title:k_havoric
		}
		title:d_silverforge = {
			set_de_jure_liege_title = title:k_havoric
		}
	if = {
		limit = {
			title:d_ibevar = { holder = root }
		}
		title:d_ibevar = { set_de_jure_liege_title = title:k_havoric }
	}
	if = {
		limit = {
			title:d_larthan = { holder = root }
		}
		title:d_larthan = { set_de_jure_liege_title = title:k_havoric }
	}
	if = {
		limit = {
			title:d_nurael = { holder = root }
		}
		title:d_nurael = { set_de_jure_liege_title = title:k_havoric }
	}
	}
	hidden_effect = {
		title:d_asheniande = {
			set_de_jure_liege_title = title:k_havoric

		}
		title:d_bennon = {
			set_de_jure_liege_title = title:k_havoric

		}
		title:d_silverforge = {
			set_de_jure_liege_title = title:k_havoric

		}
		if = {
			limit = {
				title:d_ibevar = { holder = root }
			}
			title:d_ibevar = { set_de_jure_liege_title = title:k_havoric }

		}
		if = {
			limit = {
				title:d_larthan = { holder = root }
			}
			title:d_larthan = { set_de_jure_liege_title = title:k_havoric }

		}
		if = {
			limit = {
				title:d_nurael = { holder = root }
			}
			title:d_nurael = { set_de_jure_liege_title = title:k_havoric }

		}
	}
}
