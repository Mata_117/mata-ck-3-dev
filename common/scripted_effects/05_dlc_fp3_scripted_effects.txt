﻿#Anbennar - we've just emptied these effects 

##################################################
# Struggle & Struggle Decisions
##################################################

##### Struggle Ending #####
fp3_end_persian_struggle_effect = { } # Anbennar

#### MISC STRUGGLE ENDING(s) STUFF ####
fp3_ending_effects_assertion = { } # Anbennar

fp3_struggle_ending_shia_caliphate_effects = { } # Anbennar

fp3_struggle_ending_vassalize_caliph_effects = { } # Anbennar

#### REMOVE CONTRACT COOLDOWN ####
fp3_remove_vassal_contract_cooldown_for_tension_effect = { } # Anbennar

fp3_struggle_apply_independent_vassalage_catalyst_effect = { } # Anbennar

fp3_struggle_apply_supporter_detractor_war_won_catalysts_effect = { } # Anbennar

###################################
# MISC Effects
###################################
remove_any_education_traits_effect = { # Looks ugly but performance is better than with an IF check
	remove_trait = education_martial_1
	remove_trait = education_martial_2
	remove_trait = education_martial_3
	remove_trait = education_martial_4
	remove_trait = education_martial_prowess_1
	remove_trait = education_martial_prowess_2
	remove_trait = education_martial_prowess_3
	remove_trait = education_martial_prowess_4
	remove_trait = education_learning_1
	remove_trait = education_learning_2
	remove_trait = education_learning_3
	remove_trait = education_learning_4
	remove_trait = education_intrigue_1
	remove_trait = education_intrigue_2
	remove_trait = education_intrigue_3
	remove_trait = education_intrigue_4
	remove_trait = education_stewardship_1
	remove_trait = education_stewardship_2
	remove_trait = education_stewardship_3
	remove_trait = education_stewardship_4
	remove_trait = education_diplomacy_1
	remove_trait = education_diplomacy_2
	remove_trait = education_diplomacy_3
	remove_trait = education_diplomacy_4
}

fp3_struggle_ending_concession_effects = { } # Anbennar


	# Strong independent iranian ruler becomes persian emperor, frees all the iranian subjects and has an easier time vassalizing and or converting them
fp3_struggle_rekindle_iran_effects = { } # Anbennar

fp3_sundered_caliphate_effects = { } # Anbennar

fp3_struggle_catalysts_for_activities_effect = {
	scope:activity = {
		if = {
			limit = {
				any_special_guest = {
					always = yes
				}
			}
			random_special_guest = {
				save_scope_as = special_honorary_guest
			}
		}
	}
	if = {
		limit = {
			this = scope:host
			scope:special_honorary_guest ?= { is_alive = yes }
			scope:host = {
				is_alive = yes
				any_character_struggle = {
					involvement = involved
					activate_struggle_catalyst_secondary_character_involvement_involved_trigger = {
						CATALYST = catalyst_invite_involved_as_honorary_guests_to_feast_hunt
						CHAR = scope:special_honorary_guest
					}
				}
			}
		}
		scope:host = {
			every_character_struggle = {
				involvement = involved
				limit = { phase_has_catalyst = catalyst_invite_involved_as_honorary_guests_to_feast_hunt }
				activate_struggle_catalyst = {
					catalyst = catalyst_invite_involved_as_honorary_guests_to_feast_hunt
					character = scope:host
				}
				log_debug_variable_for_persian_struggle_effect = { VAR = stabil_catalyst_invite_involved_as_honorary_guests_to_feast_hunt }
			}
		}
	}
}

fp3_challenge_house_head_duel_challenger_win_prestige_effect = {
	scope:house_challenger = { add_prestige = medium_prestige_gain }
}

fp3_challenge_house_head_duel_challenger_win_house_effect = {
	scope:house_challenger.house = { set_house_head = scope:house_challenger }
	scope:house_challenger = {
		custom_tooltip = {
			text = fp3_challenge_house_head_recent_cooldown_tt
			add_character_flag = {
				flag = fp3_challenge_house_head_recent_flag
				years = 3
			}
		}
	}
}

fp3_challenge_house_head_duel_challenger_loss_effect = {
	scope:house_challenger = {
		if = {
			limit = { is_alive = yes }
			add_prestige = medium_prestige_loss
		}
	}
	scope:house_head = {
		add_prestige = medium_piety_value
		if = {
			limit = {
				scope:house_challenger = { is_alive = yes }
			}
			add_hook_no_toast = {
				type = trial_by_combat_hook
				target = scope:house_challenger
			}
		}
		custom_tooltip = {
			text = fp3_challenge_house_head_recent_cooldown_tt
			add_character_flag = {
				flag = fp3_challenge_house_head_recent_flag
				years = 3
			}
		}
	}
}

# Saves Sunni holder, or the last one
fp3_save_sunni_caliph_or_previous_effect = { } # Anbennar
