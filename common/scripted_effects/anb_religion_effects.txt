﻿mark_faiths_to_found = {
	faith:regent_court = { 
		set_variable = { name = to_found }
		set_variable = { name = block_conversion_till_decision_taken }
	}
}

found_faith_no_conversion = {
	$FAITH$ = {
		remove_variable = to_found
		custom_tooltip = refound_faith_tooltip
	}
}
# Used to set a Hindu's personal god.
set_bhakti_effect = {
	# ANBENNAR
	if = {
		limit = { religion = religion:lencori_religion }
		if = {	
			limit = {
				has_character_modifier = artanos_deity
			}
			remove_character_modifier = artanos_deity
			add_piety = massive_piety_loss
		}
		else_if = {	
			limit = {
				has_character_modifier = merisse_deity
			}
			remove_character_modifier = merisse_deity
			add_piety = massive_piety_loss
		}
		else_if = {	
			limit = {
				has_character_modifier = trovecos_deity
			}
			remove_character_modifier = trovecos_deity
			add_piety = massive_piety_loss
		}
		else_if = {	
			limit = {
				has_character_modifier = careslobos_deity
			}
			remove_character_modifier = careslobos_deity
			add_piety = massive_piety_loss
		}
		else_if = {	
			limit = {
				has_character_modifier = asmirethin_deity
			}
			remove_character_modifier = asmirethin_deity
			add_piety = massive_piety_loss
		}
		else_if = {	
			limit = {
				has_character_modifier = damarta_deity
			}
			remove_character_modifier = damarta_deity
			add_piety = massive_piety_loss
		}
		else_if = {	
			limit = {
				has_character_modifier = belouina_deity
			}
			remove_character_modifier = belouina_deity
			add_piety = massive_piety_loss
		}
		else_if = {	
			limit = {
				has_character_modifier = dercanos_deity
			}
			remove_character_modifier = dercanos_deity
			add_piety = massive_piety_loss
		}
		else_if = {	
			limit = {
				has_character_modifier = sorbodua_deity
			}
			remove_character_modifier = sorbodua_deity
			add_piety = massive_piety_loss
		}
		else_if = {	
			limit = {
				has_character_modifier = turanos_deity
			}
			remove_character_modifier = turanos_deity
			add_piety = massive_piety_loss
		}
	}
	else = {
		# If no current bhakti, cost is reduced.
		add_piety = medium_piety_loss
	}

	add_character_modifier = $BHAKTI$
}