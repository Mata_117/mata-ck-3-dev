﻿dwarven_hold = {
	color = { 255 198 0 }
	
	province_modifier = {
		supply_limit_mult = 0.6
		development_growth_factor = 0.25
	}

	defender_combat_effects = {
		name = combat_dwarven_hold
		image = combat_dwarven_hold
		advantage = 12
	}
	
	movement_speed = 0.8
	combat_width = 0.8
	
	audio_parameter = 1.0
}

dwarven_hold_surface = {
	color = { 255 198 80 }
	
	province_modifier = {
		supply_limit_mult = 0.6
		development_growth_factor = 0.25
	}

	defender_combat_effects = {
		name = combat_dwarven_hold
		image = combat_dwarven_hold
		advantage = 12
	}
	
	movement_speed = 0.8
	combat_width = 0.8
	
	audio_parameter = 1.0
}