﻿### Magic Lifestyle ###
add_magic_lifestyle_xp = {
	global = GAINS_MAGIC_LIFESTYLE_XP
	global_past = GAINED_MAGIC_LIFESTYLE_XP
	first = I_GAIN_MAGIC_LIFESTYLE_XP
	first_past = I_GAINED_MAGIC_LIFESTYLE_XP
	third = TARGET_GAINS_MAGIC_LIFESTYLE_XP
	third_past = TARGET_GAINED_MAGIC_LIFESTYLE_XP
}

add_magic_lifestyle_perk_points = {
	global = GAINS_A_MAGIC_LIFESTYLE_PERKPOINT
	global_past = GAINED_A_MAGIC_LIFESTYLE_PERKPOINT
	first = I_GAIN_MAGIC_A_LIFESTYLE_PERKPOINT
	first_past = I_GAINED_A_MAGIC_LIFESTYLE_PERKPOINT
	third = TARGET_GAINS_A_MAGIC_LIFESTYLE_PERKPOINT
	third_past = TARGET_GAINED_A_MAGIC_LIFESTYLE_PERKPOINT
}
compel_personality_change = {
	global = compel_personality_change
	global_past = compel_personality_change
	first = compel_personality_change
	first_past = compel_personality_change
}