﻿KnightCulture = {
	type = character

	#ANBENNAR - FYI, none of this is used. For now it all defaults to knight_default which we are calling Hero

	
	text = {
		localization_key = knight_default
		fallback = yes
	}
}

KnightCulturePlural = {
	parent = KnightCulture
	suffix = "_plural"
}

KnightCultureNoTooltip = {
	parent = KnightCulture
	suffix = "_no_tooltip"
}

KnightCulturePluralNoTooltip = {
	parent = KnightCulture
	suffix = "_no_tooltip_plural"
}

KnightCultureNoTooltipLowercase = {
	parent = KnightCulture
	suffix = "_no_tooltip_lowercase"
}

KnightCulturePluralNoTooltipLowercase = {
	parent = KnightCulture
	suffix = "_no_tooltip_lowercase_plural"
}

KnightCulturePluralPossessiveNoTooltipLowercase = {
	parent = KnightCulture
	suffix = "_no_tooltip_lowercase_plural_possessive"
}

KnightCultureAdjectiveNoTooltipLowercase = {
	parent = KnightCulture
	suffix = "_no_tooltip_lowercase_adjective"
}
