﻿on_quest_modifier = {
	icon = prowess_positive
	monthly_income_mult = -0.1
	general_opinion = -5
	fertility = -0.10
	monthly_prestige = 0.50
}

accepted_adventurer = {
	monthly_prestige = -0.15
}

influx_of_adventurer_business = {
	icon = stewardship_positive
	tax_mult = 0.15
}

influx_of_adventurer_business_high_prices = {
	icon = stewardship_positive
	tax_mult = 0.25
	county_opinion_add = -5
}

influx_of_adventurer_business_adventurer = {
	icon = stewardship_positive
	tax_mult = 0.15
	development_growth = 0.02
}

influx_of_adventurer_labour = {
	icon = stewardship_positive
	build_speed = 0.10
	build_gold_cost = -0.05
	build_piety_cost = -0.05
	build_prestige_cost = -0.05
	monthly_county_control_change_add = -0.01
}

adventurer_mercenaries = {
	icon = martial_positive
	mercenary_hire_cost_mult = -0.1
}

adventurer_damaged_building = {
	icon = stewardship_negative
	build_speed = -0.30
}

rallied_peasants = {
	icon = martial_positive
	
	levy_reinforcement_rate = 0.05
	tax_mult = -0.05
}

army_quest_presence = {
	icon = marital_positive
	
	levy_reinforcement_rate = 0.05
}