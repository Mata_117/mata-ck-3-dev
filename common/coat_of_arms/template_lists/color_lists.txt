﻿# Anbennar - why did we edit the normal_colors?

color_lists = {
	all_color_list = { # PM : basically the average of medieval frequencies in roll of arms (strong bias towards 13th c), with values cut in half
		30 = "red"
		12 = "blue"
		1 = "green"
		14 = "black"
		0 = "purple"
		24 = "yellow"
		26 = "white"
		
		#Added by Anbennar - dunno why these boys are here tbh. Do we need them as this influences vanilla. Is it needed to refer below??
		0 = "verne_wyvern_red"
		0 = "pearlsedger_blue"
		0 = "damerian_blue"
		2 = "black"
		12 = "damerian_black"
		2 = "purpure"
		0 = "yellow_light"
		6 = "white"
		20 = "damerian_white"
		0 = "verne_beige"
		6 = "beige"
		1 = "dark_hunter_green"
	}	
	
	normal_colors = {
		30 = "red"
		12 = "blue"
		1 = "green"
		14 = "black"
		2 = "purpure"
		special_selection = {
			trigger = {
				OR = {
					coa_damerian_trigger = yes
					coa_old_damerian_trigger = yes
				}
			}
			-1 = "red"
			8 = "lorentish_red_dark"
			-1 = "blue"
			65 = "damerian_blue"
			48 = "damerian_black"
			14 = "purpure"
		}
		special_selection = {
			trigger = {
				coa_vernman_trigger = yes
			}
			0 = "red"
			30 = "verne_wyvern_red"
			-1 = "purpure"
		}
		special_selection = {
			trigger = {
				OR = {
					coa_bulwari_trigger = yes
					coa_sun_elvish_trigger = yes
				}
			}
			1 = "red"
			20 = "verne_wyvern_red"
			1 = "blue"
			80 = "green"
			1 = "damerian_black"
			5 = "purpure"
			100 = rgb { 166 205 221 } #blue sky
			20 = rgb { 113 184 198 } #blue light
			80 = rgb { 57 81 163 } #blue
			60 = rgb { 114 171 93 } #green light
			1 = rgb { 148 101 44 } #light brown
		}
	}
	metal_colors = {
		24 = "yellow"
		26 = "white"
		5 = "damerian_white"
		6 = "beige"
		special_selection = {
			trigger = {
				OR = {
					coa_damerian_trigger = yes
					coa_old_damerian_trigger = yes
				}
			}
			0 = "yellow"
			3 = "yellow_light"
			0 = "white"
			46 = "damerian_white"
			20 = "beige"
		}
		special_selection = {
			trigger = {
				coa_vernman_trigger = yes
			}
			0 = "white"
			0 = "damerian_white"
			20 = "verne_beige"
			0 = "beige"
		}
		special_selection = {
			trigger = {
				OR = {
					coa_bulwari_trigger = yes
					coa_sun_elvish_trigger = yes
				}
			}
			0 = "yellow"
			23 = "yellow_light"
			6 = rgb { 219 128 78 } #orange
			0 = "white"
			0 = "damerian_white"
			14 = rgb { 206 197 197 } #white
		}
	}

	#Anbennar I think
	fire_colors = {
		15 = "orange"
		15 = "yellow_light"
		5 = "damerian_white"
		special_selection = {
			trigger = {
				OR = {
					coa_bulwari_trigger = yes
					coa_sun_elvish_trigger = yes
				}
			}
			35 = rgb { 214 81 85 } #red
			15 = rgb { 219 128 78 } #orange
			10 = "orange"
			45 = "yellow_light"
		}
	}
}
