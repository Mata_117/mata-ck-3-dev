version="v0.1"
tags={
	"Total Conversion"
}
name="anbennar-ck3-dev"
replace_path="common/culture/cultures"
replace_path="common/religion/religions"
replace_path="history/characters"
replace_path="history/provinces"
replace_path="history/titles"
replace_path="history/cultures"
replace_path="tests"
supported_version="1.11.2"
path="mod/anbennar-ck3"
